/*

    Задание 1:

    Написать объект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
function Train(name, speed, capacity) {
    this.name = name;
    this.speed = speed;
    this.capacity = capacity;
    this.go = function () {
        this.speed = 300;
        console.log(`Поезд ${this.name} везет ${this.capacity} со скоростью ${this.speed}`)
    }
    this.stop = function () {
        this.speed = 0;
        console.log(`Поезд ${name} остановился. Скорость ${speed}`)
    }
    this.pickup = function () {
        this.capasity++;
    }
}
let train = new Train("intercity Lviv - Kyiv", 100, 200);
console.log(train)
train.pickup(); train.pickup();
console.log(train)
train.stop();
console.log(train)
train.go();
console.log(train)