/*

    Задание 2.

    Напишите 3 функции, которые изменяют цвет фона и цвет текста, присваивая к новым цветам
    значение из this.color, this.background.
    А также выводят картинку с иконкой на экран.

    1. Ф-я принимает цвет фона, а цвет текста и картинка попадают в неё через метод .call(obj)
    2. Ф-я не принимает никаких аргументов, а необходимые настройки полностью передаются через bind
    3. Ф-я принимает картинку, а объект с настройками цветов передаем через .apply();

*/
let colors = {
  background: 'purple',
  color: 'white'
}

const houseImgScr = "https://image.flaticon.com/icons/svg/149/149064.svg"

let image = new Image(300, 300);



image.src = houseImgScr;


function f1(backgroundColor) {
  let element = document.body;
  element.style.color = this.color
  element.style.backgroundColor = backgroundColor
  element.appendChild(this.image)
}

let f1Obj = {
  color: colors.color,
  image: image
}


function f2() {
  let element = document.body;
  element.style.color = this.color
  element.style.backgroundColor = this.background
  element.appendChild(this.image)
}

let f2Obj = {
  color: colors.color,
  background: colors.background,
  image: image
}

let bindedf2 = f2.bind(f2Obj);



function f3(img) {
  let element = document.body;
  element.style.color = this.color
  element.style.backgroundColor = this.background
  element.appendChild(img)
}

f1.call(f1Obj, colors.background)
  // bindedf2()
  // f3.apply(colors, [image])