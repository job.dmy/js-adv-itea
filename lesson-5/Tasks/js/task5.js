/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

    2. Создать прототип, в котором будет содержаться ссылка на картинку по умолчанию, а
    так же метод который увеличивает счетчик лайков.

    let myComment1 = new Comment(...);

    3. Создать массив из 4х комментариев.
    (Подумайте, как их туда можно добавить в автоматическом режиме имея только массив)
    var CommentsArray = [myComment1, myComment2...]

    4. Создать функцию, которая принимает массив комментариев.
    И выводит каждый из них на страничку.

    Бонус:
    4. У каждого комментария, должна быть кнопка, при нажатии на которую будет вызываться метод
    addLike() и перересовыватся кол-во лайков на уже отрисованом комменте.
*/

function Comment(name, text, avatarUrl) {
  this.name = name;
  this.text = text;
  this.avatarUrl = avatarUrl;
  this.likes = 0;
}

let commentPrototype = {
  defaultAvatarUrl: "https://image.flaticon.com/icons/svg/149/149064.svg",
  addLike: function (event) {
    this.likes++;
    event.target.closest(".comment").querySelector(".likes").innerText = this.likes
  }
}


let myComment1 = new Comment('Cheburashka', "myComment1 text");
Object.setPrototypeOf(myComment1, commentPrototype)
console.log(myComment1)


var CommentsArray = []
for (let i = 0; i < 4; i++) {
  let myComment = new Comment("myComment" + (i + 1), "myComment" + (i + 1) + " text")
  Object.setPrototypeOf(myComment, commentPrototype)
  CommentsArray.push(myComment)
}

console.log(CommentsArray)

function showComments(commentsArray) {
  let container = document.createElement("div")
  container.classList.add("container")

  for (key in commentsArray) {
    let comment = commentsArray[key];

    let div = document.createElement("div")
    div.dataset.id = key
    div.classList.add("comment")

    let spanName = document.createElement("span")
    spanName.classList.add("name")
    spanName.innerText = comment.name

    let spanText = document.createElement("span")
    spanText.classList.add("text")
    spanText.innerText = comment.text

    let spanLikes = document.createElement("span")
    spanLikes.classList.add("likes")
    spanLikes.innerText = comment.likes
    spanLikes.id = "like-" + key;

    let img = document.createElement("img")
    if (typeof (comment.avatarUrl) !== "undefined") {
      img.src = comment.avatarUrl
    } else {
      img.src = comment.defaultAvatarUrl
      console.log(comment.defaultAvatarUrl)
    }

    let button = document.createElement("button")
    button.classList.add("like-button")
    button.innerText = "Like"

    let bindedHandler = comment.addLike.bind(comment)
    button.addEventListener("click", bindedHandler)

    div.appendChild(spanName)
    div.appendChild(spanText)
    div.appendChild(spanLikes)
    div.appendChild(button)
    div.appendChild(img)

    container.appendChild(div)
  };

  document.body.appendChild(container)
}

showComments(CommentsArray)