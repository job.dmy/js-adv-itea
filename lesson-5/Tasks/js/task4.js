/*

	Задание "Шифр цезаря":

	https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

	Написать функцию, которая будет принимать в себя слово и количество
	символов на которые нужно сделать сдвиг внутри.

	Написать функцию дешифратор которая вернет слово в изначальный вид.

	Сделать статичные функции используя bind и метод частичного
	вызова функции (каррирования), которая будет создавать и дешифровать
	слова с статично забитым шагом от одного до 5.

	Например:
		encryptCesar( 3, 'Word');
		encryptCesar1(...)
		...
		encryptCesar5(...)

		decryptCesar1(3, 'Sdwq');
		decryptCesar1(...)
		...
		decryptCesar5(...)

		"а".charCodeAt(); // 1072
		String.fromCharCode(189, 43, 190, 61) // ½+¾
*/

function encryptCesar(shift, word) {
	let resultWord = ""
	for (i = 0; i < word.length; i++) {
		resultWord += String.fromCharCode(word[i].charCodeAt() + shift)
	}

	return resultWord;
};

function decryptCesar(shift, word) {
	let resultWord = ""
	for (i = 0; i < word.length; i++) {
		resultWord += String.fromCharCode(word[i].charCodeAt() - shift)
	}

	return resultWord;
};

let someWord = "Hello world"
console.log(someWord)
let encryptedWord = encryptCesar(3, someWord)
console.log(encryptedWord)
console.log(decryptCesar(3, encryptedWord))

for (let i = 1; i <= 5; i++) {
	eval(`window["encryptCesar${i}"] = encryptCesar.bind( null, ${i});`)
	eval(`window["decryptCesar${i}"] = decryptCesar.bind(null, ${i})`)
}


let encrypted1Word = encryptCesar3(someWord)
console.log(encrypted1Word)
console.log(decryptCesar3(encrypted1Word))