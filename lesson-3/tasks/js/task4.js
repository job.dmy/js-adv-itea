/*
	Задание написать простой слайдер.

	Есть массив с картинками из которых должен состоять наш слайдер.
	По умолчанию выводим первый элемент нашего слайдера в блок с id='slider'
	( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
	По клику на PrevSlide/NextSlide показываем предыдущий или следующий слайд соответственно.

	Для этого необходимо написать 4 функции которые будут:

	1. Срабатывать на событие load объекта window и загружать наш слайд по умолчанию.
	2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
	3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
	4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
		+ бонус сделать так чтобы при достижении последнего и первого слайда вас после кидало на первый и последний слайд соответственно.
		+ бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

let OurSliderImages = ['images/cat1.jpg', 'images/cat2.jpg', 'images/cat3.jpg', 'images/cat4.jpg', 'images/cat5.jpg', 'images/cat6.jpg', 'images/cat7.jpg', 'images/cat8.jpg'];
let currentPosition = 0;
window.addEventListener('load', function(){
	const slider = document.getElementById('slider');
	const prevButton = document.getElementById('PrevSilde')
	const nextButton = document.getElementById('NextSilde')
	prevButton.addEventListener("click", movePrev)
	nextButton.addEventListener("click", moveNext)
	slider.style.position = "relative"

	createStyles()

	let imageToLoad = new Image(300, 300);
	imageToLoad.style.position = "absolute"
	imageToLoad.onload = function(){
		slider.appendChild(imageToLoad);
	}; 

	imageToLoad.onerror = function(err){
		new Error('Some Error on load', err);
	};

	imageToLoad.src = OurSliderImages[currentPosition]; 

})
function moveNext(e){
	currentPosition++
	if(!OurSliderImages[currentPosition]){
		currentPosition = 0;
	}
	renderImage(currentPosition, OurSliderImages)
}
function movePrev(e){
	currentPosition--;
	if(!OurSliderImages[currentPosition]){
		currentPosition = OurSliderImages.length -1;
	}
	renderImage(currentPosition, OurSliderImages)
}

function renderImage(currentPosition, OurSliderImages) {
	let oldImg = slider.querySelector("img");

	let imageToLoad = new Image(300, 300);

	imageToLoad.style.position = "absolute"
	imageToLoad.classList.add("top")
	imageToLoad.onload = function(){

		setTimeout(function(){ 
			removeAllChildNodes(slider)
			imageToLoad.classList.remove("top")
			slider.appendChild(imageToLoad);
		}, 1000);
		 
		slider.appendChild(imageToLoad);
	}; 

	imageToLoad.onerror = function(err){
		new Error('Some Error on load', err);
	};

	imageToLoad.src = OurSliderImages[currentPosition]; 
}

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

function createStyles(){
const	cssText = `
.top {
	animation-name: fade;
	animation-timing-function: ease-in-out;
	animation-iteration-count: infinite;
	animation-duration: 1s;
	animation-direction: alternate;
  }
  
  @keyframes fade {
	0% {
	  opacity: 1;
	}
	25% {
	  opacity: 1;
	}
	75% {
	  opacity: 0;
	}
	100% {
	  opacity: 0;
	}
  }
`;
let style = document.createElement("style");
 
let css = document.createTextNode(cssText);

style.type = 'text/css';
style.appendChild(css);
document.head.appendChild(style)
}