/*

  Задание 1.

  Написать функцию которая будет задавать СЛУЧАЙНЫЙ цвет для фона.
  Каждая перезагрузка страницы будет с новым цветом.
  Для написания используйте функцию на получение случайного целого числа,
  между минимальным и максимальным значением (Приложена снизу задания)

  + Бонус, повесить обработчик на кнопку через метод onClick
  + Бонус, использовать 16-ричную систему исчесления и цвет HEX -> #FFCC00
  + Бонус выводить полученый цвет по центру страницы.
  
  Необходимо создать блок через createElement задать ему стили через element.style
  и вывести через appendChild или insertBefore

  Необходимые материалы:
    Math.Random (Доки): https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    __
    Работа с цветом:
    Вариант 1.
      Исользовать element.style.background = 'rgb(r,g,b)';
      где r,g,b случайное число от 0 до 255;

    Вариант 2.
      Исользовать element.style.background = '#RRGGBB';
      где, RR, GG, BB, значние цвета в 16-ричной системе исчесления
      Формирование цвета в вебе: https://ru.wikipedia.org/wiki/%D0%A6%D0%B2%D0%B5%D1%82%D0%B0_HTML
      Перевод в 16-ричную систему исчесления делается при помощи
      метода Number.toString( 16 ) https://www.w3schools.com/jsref/jsref_tostring_number.asp,

      var myNumber = '251'
      myNumber.toString(16) // fb
*/

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

document.body.parentElement.style.height = '100%';
document.body.style.height = '100%';

const appElement = document.getElementById('app');
appElement.style.height = '100%';
appElement.style.position = 'relative';

const coloredDiv = document.createElement('div');
coloredDiv.style.textAlign = 'center';
coloredDiv.style.width = '50%';
coloredDiv.style.height = '50%';

coloredDiv.style.margin = 'auto';
coloredDiv.style.position = 'absolute';
coloredDiv.style.top = '25%';
coloredDiv.style.left = '25%';

coloredDiv.style.border = '3px solid gray';

appElement.appendChild(coloredDiv);

const targetElement = coloredDiv;

function setRandomColor1(element = targetElement) {
  const min = 0;
  const max = 255;

  const r = getRandomIntInclusive(min, max);
  const g = getRandomIntInclusive(min, max);
  const b = getRandomIntInclusive(min, max);

  const rgbString = 'rgb('+ r + ', ' + g + ', ' + b + ')';
  console.log('rgbString', rgbString);
  
  element.style.backgroundColor = rgbString;
}

function setRandomColor2(element = targetElement) {
  const min = 0;
  const max = 255;

  const r = getRandomIntInclusive(min, max);
  const g = getRandomIntInclusive(min, max);
  const b = getRandomIntInclusive(min, max);

  const hexString = '#' + r.toString(16) + g.toString(16) + b.toString(16);
  console.log('hexString', hexString);
  element.style.backgroundColor = hexString;
}

const buttons = document.createElement('div');

const button1 = document.createElement('button');
button1.innerText = 'Change color 1';
button1.setAttribute('onclick', 'setRandomColor1()');

const button2 = document.createElement('button');
button2.innerText = 'Change color 2';
button2.setAttribute('onclick', 'setRandomColor2()');

buttons.appendChild(button1);
buttons.appendChild(button2);


appElement.insertBefore(buttons, targetElement);

setRandomColor2();
// setRandomColor1();