
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : !"23123", "age": 15, "password": "*****" }'


*/
document.addEventListener("DOMContentLoaded", () => {
  let formString = `
<form id="form1">
  <input name="name" id="name"/>
  <input name="age" id="age"/>
  <input name="password" type="password" id="password"/>
  <button id="form1btn">form to json</button>
</form>

<form  id="form2">
  <input />
  <button id="form2btn">JSON.parse()</button>
</form>`;

  let wrapper = document.createElement("div");
  wrapper.id = "wrapper";
  wrapper.innerHTML = formString.trim();
  console.log(wrapper);
  document.body.appendChild(wrapper);

  let button1 = document.getElementById("form1btn");
  let button2 = document.getElementById("form2btn");
  button1.addEventListener("click", e => {
    e.preventDefault();
    let form1Array = Array.from(form1);
    let inputs = [];
    form1Array.forEach((item, index) => {
      if (item.nodeName == "INPUT") {
        inputs.push(item);
      }
    });

    let form1Json = {}

    inputs.forEach(item => {
      form1Json[item.name] = item.value;
    });
    let stringified = JSON.stringify(form1Json);
    console.log(stringified)
  });

  button2.addEventListener("click", e => {
    e.preventDefault();
    let parsed = null;
    try {
      parsed = JSON.parse(document.getElementById("form2").querySelector("input").value);
    } catch {

    }
    if (parsed !== null) {
      console.log(parsed);
    }

  })
});